## Server.js
- run export_ENV=test (for testing and testing database todo_test)
- run exort_ENV=development(for development database mydb)

## Routes
- all the routes add,update,delete,retrive,show for user and task

## models
- users.js for user table having id,name,email,password attributes
- task.js for task table having task_id,task_title,task_about,task_status,expected_date,completed_date,user_id

## migrations
- migration for user.js
- migration for task.js

## seeders
- running sample seed 
export_ENV=test
npx sequelize-cli db:seed:all

## assets
- jquery.js static file for using jquery
- api-docs for swagger

## test
- routes.test.js testing using jest
- command for run npm test

## swagger
- command for run node server.js
- in browser localhost:1585/api-docs

## .env 
- file for contains SESSION_SECRET
