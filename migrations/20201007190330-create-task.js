'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('task', {

      task_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      task_title: {
        type: Sequelize.STRING
      },
      task_about: {
        type: Sequelize.STRING
      },
      task_status: {
        type: Sequelize.STRING
      },
      expected_date: {
        type: Sequelize.DATE
      },
      completed_date: {
        type: Sequelize.DATE
      },
      user_id: {
        type: Sequelize.INTEGER
      }
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('task')
  }
}
