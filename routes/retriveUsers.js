'use strict'
const express = require('express')
const router = express.Router()

const Mydb = require('../models/index')

router
  .route('/')
  .get(async (req, response) => {
    // console.log(query);
    await Mydb.users
      .findAll()
      .then(res => {
        response.json((res.rows))
      })
      .catch(err => {
        response.write(err)
      })
      .finally(() => {
        response.status(200)
        response.end()
      })
  })

module.exports = router
