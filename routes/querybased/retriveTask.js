/**
 * @swagger
 * /retriveTask?task_id={task_id}:
 *   get:
 *     tags:
 *       - Tasks
 *     description: Returns a single Task
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: task_id
 *         description: Task's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: A single Task
 *         schema:
 *           $ref: '#/definitions/Tasks'
 */
 /**
 * @swagger
 * definitions:
 *   Tasks:
 *     properties:
 *       task_id:
 *         type: integer
 *       task_name:
 *         type: string
 */

 "use strict";
const express=require("express");
let router=express.Router();



const { Client } = require('pg');
const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'mydb',
    password: '1234',
    port: 5432,
});

client.connect();
 router
 .route('/')
 .get((req, response) => {

        const query = "SELECT * FROM task WHERE task_id="+req.query.task_id+";";
        //console.log(query);
        client
            .query(query)
            .then(res => {
                response.json((res.rows[0]));
            })
            .catch(err => {
                response.write(err);
            })
            .finally(() => {
                response.status(200);
               response.end();
            });              
	})

module.exports=router;