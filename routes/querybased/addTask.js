"use strict";
const express=require("express");
let router=express.Router();

var bodyParser = require('body-parser');



router.use(bodyParser.urlencoded({
  extended: true
}));
router.use(bodyParser.json());

const config=require('../config/default.js');
const { Client } = require('pg');//module for postgreSQL
const client = new Client(config.database);//add config for database
client.connect();


 router
 .route('/')
 .post((req, response) => {
           
        const query = "INSERT INTO task (task_title,task_about,task_status,expected_date,completed_date,user_id) VALUES ('"+req.body.task_title+"','"+req.body.task_about+"','"+req.body.task_status+"','"+req.body.expected_date+"','"+req.body.completed_date+"',1);";
        //console.log(req.user);

     var output={};       
    client
            .query(query)
            .then(res => {
                output.status=true; 
                output.message="data inserted";
                response.status(201);
                response.json(output);
                
            })
            .catch(err => {
                output.status=false; 
                output.message="data not inserted";
                response.status(400);
                response.json(output);
                
            
            })
            .finally(() => {
                
               response.end();
            });          
    })

module.exports=router;