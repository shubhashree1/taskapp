/**
 * @swagger
 * /deleteTask?task_id={id}:
 *   delete:
 *     tags:
 *       - Tasks
 *     description: Deletes a single Task
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Task's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       204:
 *       - name: status
 *         description: True/False
 *         in: path
 *         required: true
 *         type: boolean
 *       - name: message
 *         description: message of operation
 *         in: path
 *         required: true
 *         type: string
 */


"use strict";
//const config = require("config");

const express=require("express");
const router=express.Router();

const config=require('../config/default.js');
const { Client } = require('pg');//module for postgreSQL
const client = new Client(config.database);//add config for database
client.connect();
 
 router
 .route('/')
 .delete((req, response) => {
    //console.log(req.query.task_id);
    //response.write("in deleteTask"); 

    const query = "DELETE FROM task WHERE task_id="+req.query.task_id+";";
    
    client.query(query, (err, res) => 
      {
           
           //console.log(res.rowCount);

          var output = {};
          if (err) 
          {
             output.status = false;
             output.message = 'Unsuccessful Delete Operation.'
             //console.error(err);
             response.status(424);
              response.json(output);
              response.end(); 
             return
          }
          if(res.rowCount==1)
          {
                    output.status = true;
                    output.message = 'successfully Deleted.'
                    response.status(200);
                    
          }
          else
          {
              output.status = false;
              output.message = 'task not found.'
              response.status(400);
          }
          
         response.json(output);
          response.end();  
      });           
  });

module.exports=router;