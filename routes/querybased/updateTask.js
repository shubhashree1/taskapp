
/**
 * @swagger
 * /updateTask?task_id={task_id}&task_title={task_title}&task_about={task_about}&task_status={task_status}&expected_date={expected_date}&completed_date={completed_date}:
 *   post:
 *     tags:
 *       - Tasks
 *     description: update a task
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: task_id
 *         description: task id
 *         in: body
 *         required: true
 *         type: integer
 *       - name: task_title
 *         description: task name
 *         in: body
 *         required: true
 *         type: string
 *       - name: task_about
 *         description: task discription
 *         in: body
 *         required: true
 *         type: string
 *       - name: task_status
 *         description: task status
 *         in: body
 *         required: true
 *         type: string
 *         enum: [1,2]
 *       - name: expected_date
 *         description: expected date to complete task
 *         in: body
 *         required: true
 *         type: date
 *       - name: completed_date
 *         description: actual completion date
 *         in: body
 *         required: true
 *         type: date
 *         schema:
 *           $ref: '#/definitions/Tasks'
 *     responses:
 *       200:
 *         description: Successfully updated
 */
 /**
 * @swagger
 * definitions:
 *   Tasks:
 *     properties:
 *       task_id:
 *         type: integer
 *       task_name:
 *         type: string
 */
"use strict";
const express=require("express");
let router=express.Router();
//var config = require("config");

var bodyParser = require('body-parser');



router.use(bodyParser.urlencoded({
  extended: true
}));
router.use(bodyParser.json());


const config=require('../config/default.js');
const { Client } = require('pg');//module for postgreSQL
const client = new Client(config.database);//add config for database
client.connect();

 
 router
 .route('/')
 .post((req, response) => {
 	//console.log(req.body.task_id);
    var output = {};
 	//response.write('data updated');

    const query = "UPDATE task SET task_title='"+req.body.task_title+"',task_about='"+req.body.task_about+"',task_status='"+req.body.task_status+"',expected_date='"+req.body.expected_date+"',completed_date='"+req.body.completed_date+"' WHERE task_id="+req.body.task_id+";";
    //console.log(query);
    client.query(query, (err, res) => {
    if (err) {
        console.error(err);
        return;
        }
         response.json(output);
        response.end();
        });
 })


module.exports=router;