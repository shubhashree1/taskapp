/**
 * @swagger
 * definitions:
 *   Tasks:
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       status:
 *         type: enum
 *       expected_date:
 *         type: date
 *       completed_date:
 *         type: date
 */

/**
 * @swagger
 * /showTask?page={page}&limit={limit}:
 *   get:
 *     tags:
 *       - Tasks
 *     description: Returns all Tasks
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page number
 *         in: path
 *         required: true
 *         type: integer
 *       - name: limit
 *         description: Number of records on a page
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: An array of Tasks
 *         schema:
 *           $ref: '#/definitions/Tasks'
 */

 "use strict";
//const config = require("config");
const express=require("express");
const router=express.Router();

const config=require('../config/default.js');
const { Client } = require('pg');//module for postgreSQL
const client = new Client(config.database);//add config for database
client.connect();
 
 router
 .route('/')
.get((req, response) => {
	console.log(req.user);
	const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
	var offset = (Number(page) - 1) * Number(limit);//if page=2 limit =5 than offset will be 5
    const query = 'SELECT *,(SELECT count(*) as total from task) FROM task limit '+limit+' OFFSET '+offset+';';    
        client
            .query(query)
            .then(resTask => {
                var output = {};
				if(page != '1')
				{
					output.previous = {};
					output.previous.page = Number(Number(page)-1);
					output.previous.limit = Number(limit);
				}
				if((offset+Number(limit)) < Number(resTask.rows[0].total))//if total record > offset+limit 5+5 than show next button
				{
					output.next = {};
					output.next.page = Number(Number(page)+1);
					output.next.limit = Number(limit);
				
				}
				output.result = resTask.rows;
				//console.log(JSON.stringify(output));
				response.json((output));
            })
            .catch(err => {
                response.write(err);
            })
            .finally(() => {
                response.status(200);
               response.end();
            });              
	})

   
module.exports=router;