
'use strict'
// const config = require("config");
const express = require('express')
const router = express.Router()

const Mydb = require('../models/index')

router
  .route('/')
  .get(async (req, response) => {
    const page = parseInt(req.query.page)
    const limit = parseInt(req.query.limit)
    var offset = (Number(page) - 1) * Number(limit)// if page=2 limit =5 than offset will be 5

    await Mydb.users.findAndCountAll({ offset: offset, limit: limit })
      .then(resUser => {
        var output = {}
        if (page !== 1) {
          output.previous = {}
          output.previous.page = Number(Number(page) - 1)
          output.previous.limit = Number(limit)
        }
        if ((offset + Number(limit)) < Number(resUser.rows[0].total)) { // if total record > offset+limit 5+5 than show next button
          output.next = {}
          output.next.page = Number(Number(page) + 1)
          output.next.limit = Number(limit)
        }
        output.result = resUser.rows
        // console.log(JSON.stringify(output));
        response.json((output))
      })
      .catch(err => {
        response.write(err)
      })
      .finally(() => {
        response.status(200)
        response.end()
      })
  })

module.exports = router
