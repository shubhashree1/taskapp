'use strict'
// const config = require("config");

const express = require('express')
const router = express.Router()

const Mydb = require('../models/index')

router
  .route('/')
  .delete(async (req, response) => {
    // console.log(req.query.task_id);
    var output = {}
    await Mydb.users
      .destroy({ where: { id: req.query.id } })
      .then(res => {
        if (res.rowCount === 1) {
          output.status = true
          output.message = 'successfully Deleted.'
          response.status(200)
        } else {
          output.status = false
          output.message = 'task not found.'
          response.status(400)
        }
        response.json(output)
        response.end()
      })
      .catch(error => {
        output.status = false
        output.message = 'Unsuccessful Delete Operation.'
        console.error(error)
        response.status(424)
        response.json(output)
        response.end()
      })
  })
module.exports = router
