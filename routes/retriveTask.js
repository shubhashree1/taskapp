/**
 * @swagger
 * definitions:
 *   Tasks:
 *     properties:
 *       task_title:
 *         type: string
 *       task_about:
 *         type: string
 *       task_status:
 *         type: string
 *         enum: [pending,in process,completed]
 *       expected_date:
 *         type: string
 *         format: date
 *       completed_date:
 *         type: string
 *         format: date
 */

/**
 * @swagger
 * /retriveTask?task_id={task_id}:
 *   get:
 *     tags:
 *       - Tasks
 *     description: Returns a single Task
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: task_id
 *         description: Task's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: A single Task
 *         schema:
 *           $ref: '#/definitions/Tasks'
 */

'use strict'
const express = require('express')
const router = express.Router()

const Mydb = require('../models/index')

router
  .route('/')
  .get(async (req, response) => {
    await Mydb.task
      .findOne({ where: { task_id: req.query.task_id } })
      .then(result => {
        // console.log(result);
        response.json((result))
      })
      .catch(err => {
        response.write('' + err)
        console.log(err)
      })
      .finally(() => {
        response.end()
      })
  })

module.exports = router
