/**
 * @swagger
 * definitions:
 *   Tasks:
 *     properties:
 *       task_id:
 *         type: integer
 *       task_title:
 *         type: string
 *       task_about:
 *         type: string
 *       task_status:
 *         type: string
 *         enum: [pending,in process,completed]
 *       expected_date:
 *         type: string
 *         format: date
 *       completed_date:
 *         type: string
 *         format: date
 */
/**
 * @swagger
 * /updateTask:
 *   put:
 *     tags:
 *       - Tasks
 *     description: update a task
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: task
 *         description: fields for task resource
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Tasks'
 *           type: array
 *     responses:
 *       200:
 *         description: Successfully updated
 */

'use strict'
const express = require('express')
const router = express.Router()

var bodyParser = require('body-parser')

router.use(bodyParser.urlencoded({
  extended: true
}))
router.use(bodyParser.json())

const Mydb = require('../models/index')

router
  .route('/')
  .put(async (req, response) => {
    console.log(req.body)
    var output = {}
    await Mydb.task
      .update({ task_title: req.body.task_title, task_about: req.body.task_about, task_status: req.body.task_status, expected_date: req.body.expected_date, completed_date: req.body.completed_date }, { where: { task_id: req.body.task_id } })
      .then(result => {
        output.status = true
        output.message = 'task updated'
        response.json(output)
      })
      .catch(err => {
        response.write('' + err)
        console.log(err)
      })
      .finally(() => {
        response.end()
      })
  })

module.exports = router
