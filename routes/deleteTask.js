/**
 * @swagger
 * /deleteTask?task_id={id}:
 *   delete:
 *     tags:
 *       - Tasks
 *     description: Deletes a single Task
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Task's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       204:
 *       - name: status
 *         description: True/False
 *         in: path
 *         required: true
 *         type: boolean
 *       - name: message
 *         description: message of operation
 *         in: path
 *         required: true
 *         type: string
 */

'use strict'
// const config = require("config");

const express = require('express')
const router = express.Router()

const Mydb = require('../models/index')

router
  .route('/')
  .delete(async (req, response) => {
    var output = {}
    try {
      const task = await Mydb.task
        .destroy({ where: { task_id: req.query.task_id } })
        // console.log(task);
      if (task) {
        output.status = true
        output.message = 'successfully Deleted.'
        response.status(200)
      } else {
        output.status = false
        output.message = 'task not found.'
        response.status(400)
      }
      response.json(output)
      response.end()
    } catch (error) {
      output.status = false
      output.message = 'Unsuccessful Delete Operation.'
      // console.error(err);
      response.status(424)
      response.json(output)
      response.end()
      // return
    }
  })

module.exports = router
