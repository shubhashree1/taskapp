/**
 * @swagger
 * definitions:
 *   Tasks:
 *     properties:
 *       task_title:
 *         type: string
 *       task_about:
 *         type: string
 *       task_status:
 *         type: string
 *         enum: [pending,in process,completed]
 *       expected_date:
 *         type: string
 *         format: date
 *       completed_date:
 *         type: string
 *         format: date
 */
/**
 * @swagger
 * /addTask:
 *   post:
 *     tags:
 *       - tasks
 *     description: Creates a new task
 *     parameters:
 *       - name: task
 *         description: task object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Tasks'
 *     responses:
 *       200:
 *         description: Successfully created
 */

'use strict'
const express = require('express')
const router = express.Router()

var bodyParser = require('body-parser')

router.use(bodyParser.urlencoded({
  extended: true
}))
router.use(bodyParser.json())

const Mydb = require('../models/index')

router
  .route('/')
  .post(async (req, response) => {
    var output = {}
    console.log(req.body)
    if (typeof req.body.task_title !== 'undefined' && req.body.task_title != null) {
      await Mydb.task
        .create({ task_title: req.body.task_title, task_about: req.body.task_about, task_status: req.body.task_status, expected_date: req.body.expected_date, completed_date: req.body.completed_date, user_id: 1 })
        .then(result => {
          console.log(result)
          output.task_id = result.dataValues.task_id
          output.status = true
          output.message = 'data inserted'
          response.status(201)
          response.json(output)
          response.end()
        })
        .catch(error => {
          output.status = false
          output.message = 'data not inserted'
          response.status(400)
          console.log(error)
          response.json(output)
          response.end()
        })
    } else {
      output.status = false
      output.message = 'data not inserted'
      response.status(400)
      response.json(output)
      response.end()
    }
  })

module.exports = router
