
/**
 * @swagger
 * /showTask?page={page}&limit={limit}:
 *   get:
 *     tags:
 *       - Tasks
 *     description: Returns all Tasks
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page
 *         description: Page number
 *         in: path
 *         required: true
 *         type: integer
 *       - name: limit
 *         description: Number of records on a page
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: An array of Tasks
 *         schema:
 *           $ref: '#/definitions/Tasks'
 */

'use strict'
// const config = require("config");
const express = require('express')
const router = express.Router()

const Mydb = require('../models/index')

router
  .route('/')
  .get(async (req, response) => {
    const page = parseInt(req.query.page)
    const limit = parseInt(req.query.limit)
    var offset = (Number(page) - 1) * Number(limit)// if page=2 limit =5 than offset will be 5
    var output = {}
    await Mydb.task
      .findAndCountAll({ offset: offset, limit: limit })
      .then(result => {
        if (result.rows.length === 0) {
          output.status = false
          output.message = 'task not found'
          response.json(output)
        } else {
          if (page !== 1) {
            output.previous = {}
            output.previous.page = Number(Number(page) - 1)
            output.previous.limit = Number(limit)
          }
          if ((offset + Number(limit)) < Number(result.count)) { // if total record > offset+limit 5+5 than show next button
            output.next = {}
            output.next.page = Number(Number(page) + 1)
            // console.log(JSON.stringify(output));
            output.next.limit = Number(limit)
          }
          output.result = result.rows
          response.json((output))
        }
      })
      .catch(err => {
        output.status = false
        output.message = 'task not found'
        response.write(err)
      })
      .finally(() => {
        response.status(200)
        response.end()
      })
  })
module.exports = router
