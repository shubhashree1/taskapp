'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }
  }
  task.init({

    task_id:
    {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    task_title: DataTypes.STRING,
    task_about: DataTypes.STRING,
    task_status: DataTypes.STRING,
    expected_date: DataTypes.DATE,
    completed_date: DataTypes.DATE,
    user_id: DataTypes.INTEGER
  },
  {
    sequelize,
    modelName: 'task',
    tableName: 'task',
    timestamps: false
  })
  return task
}
