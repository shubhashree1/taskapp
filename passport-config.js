
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')

const Mydb = require('./models/index')

function initialize (passport) {
  const authenticateUser = async (email, password, done) => {
    var user
    Mydb.users
      .findOne({ where: { email: email } })
    // console.log(query);
      .then(async (resData) => {
        if (resData != null) {
          user = resData.dataValues
        } else {
          user = null
        }
        if (user == null) {
          return done(null, false, { message: 'no user with that email' })
        }
        if (await bcrypt.compare(password, user.password)) {
          return done(null, user)
        } else {
          return done(null, false, { message: 'incorrect password' })
        }
      })
      .catch(err => {
        console.log(err)
      })
      .finally(() => {

      })
  }

  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser))
  passport.serializeUser((user, done) => done(null, user.id))
  passport.deserializeUser((id, done) => {
    Mydb.users
      .findOne({ where: { id: id } })
      .then(resData => {
        if (resData != null) { // if only 1user match with user
          return done(null, resData.dataValues)
        } else {
          return done(null, null)
        }
      })
      .catch(err => {
        console.log(err)
      })
      .finally(() => {

      })
  })
}

module.exports = initialize
