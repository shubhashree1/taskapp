'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Task', null, {})
    await queryInterface.bulkInsert('Task', [{
      task_title: 'Task 1',
      task_about: 'in task 1',
      task_status: 'pending',
      expected_date: '2020-09-09',
      completed_date: '2020-09-09'

    }], {})
    await queryInterface.bulkDelete('Users', null, {})
    await queryInterface.bulkInsert('Users',
      [{
        user_name: 'shree',
        user_email: 'shree@gmail.com',
        user_password: '$2b$10$FamGb1U7BixCP0aE4W7yiuUZ/lmK0xo993Nt.EcChhxtY65oL.dsm',
        user_type: 'user'
      },
      {
        user_name: 'shubhu',
        user_email: 'shubhu@gmail.com',
        user_password: '$2b$10$FamGb1U7BixCP0aE4W7yiuUZ/lmK0xo993Nt.EcChhxtY65oL.dsm',
        user_type: 'admin'

      }], {})
  },

  down: async (queryInterface, Sequelize) => {

    // await queryInterface.bulkDelete('product', null, {});

  }
}
