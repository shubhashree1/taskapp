/* globals describe, expect, it */
const request = require('supertest')
const app = require('../serverworking')
const faker = require('faker')
var id

describe('Post Endpoints', () => {
  it('should add tasks', async () => {
    const res = await request(app).post('/addTask').send({
      task_title: faker.name.findName(),
      task_about: faker.lorem.sentences(),
      task_status: 'pending',
      expected_date: faker.date.past(),
      completed_date: faker.date.future()
    })
    expect(res.statusCode).toEqual(201)
    expect(res.body).toHaveProperty('status')
    expect(res.body).toHaveProperty('message')
    expect(res.body.message).toEqual('data inserted')

    id = res.body.task_id
  })
  it('should not add tasks', async () => {
    const res = await request(app).post('/addTask').send({})
    expect(res.statusCode).toEqual(400)
    expect(res.body).toHaveProperty('status')
    expect(res.body).toHaveProperty('message')
    expect(res.body.message).toEqual('data not inserted')
  })
  it('Fetch tasks for page 1 and limit 2', async () => {
    const page = 1
    const limit = faker.random.number()
    const res = await request(app).get(`/showTask?page=${page}&limit=${limit}`)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('result')
  })
})
describe('Delete Endpoints', () => {
  it('should delete a record', async () => {
    const res = await request(app).delete(`/deleteTask?task_id=${id}`)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('status')
    expect(res.body).toHaveProperty('message')
    expect(res.body.message).toEqual('successfully Deleted.')
  })

  it('should not delete a record', async () => {
    const taskId = 500
    const res = await request(app).delete(`/deleteTask?task_id=${taskId}`)
    expect(res.statusCode).toEqual(400)
    expect(res.body).toHaveProperty('status')
    expect(res.body).toHaveProperty('message')
    expect(res.body.message).toEqual('task not found.')
  })
})
