// authentication with token generation
// if(process.env.NODE_ENV !== 'production'){
require('dotenv').config()
// }
const bcrypt = require('bcrypt')
const express = require('express')
const app = express()
const fs = require('fs')
const passport = require('passport')
const session = require('express-session')

app.use(express.static('assets'))

// routes
const addTask = require('./routes/addTask')// module for add record
app.use('/addTask', addTask)
const deleteTask = require('./routes/deleteTask')// module for delete record
app.use('/deleteTask', deleteTask)
const retriveTask = require('./routes/retriveTask')// module for retrive record
app.use('/retriveTask', retriveTask)
const showTask = require('./routes/showTask')// module for showing record
app.use('/showTask', showTask)
const updateTask = require('./routes/updateTask')// module for update record
app.use('/updateTask', updateTask)

const deleteUsers = require('./routes/deleteUsers')// module for showing record
app.use('/deleteUsers', deleteUsers)
const retriveUsers = require('./routes/retriveUsers')// module for retrive record
app.use('/retriveUsers', retriveUsers)
const showUsers = require('./routes/showUsers')// module for showing record
app.use('/showUsers', showUsers)

const Mydb = require('./models/index')

const initializePassport = require('./passport-config')// passport-config file exports () initialize
initializePassport(passport)

app.use(express.urlencoded({ extended: false }))

app.use(session({
  secret: process.env.SESSION_SECRET, // take SESSION_SECRET from .env file
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize())// initializing passport
app.use(passport.session())// initializing session
var swaggerJSDoc = require('swagger-jsdoc')

// swagger definition
var swaggerDefinition = {
  info: {
    title: 'Node Swagger API',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger'
  },
  host: 'localhost:1585',
  basePath: '/'
}

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.js']
}

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options)

app.use(express.static('assets'))
app.get('/swagger.json', function (req, res) {
  res.setHeader('Content-Type', 'application/json')
  res.send(swaggerSpec)
})

app.get('/', checkAuthenticated, function (req, response) {
  console.log(req.user)
  var file
  if (req.user.type === 'user') {
    file = 'index.html'
  } else {
    file = 'admin.html'
  }

  fs.readFile(file, null, function (error, data) {
    if (error) {
      response.writeHead(404)
      response.write('Whoops! File not found!')
    } else {
      response.write(data)
      console.log(req.user)
      response.end()
    }
  })
})

app.get('/login', checkNotAuthenticated, function (req, response) {
  fs.readFile('login.html', null, function (error, data) {
    if (error) {
      response.writeHead(404)
      response.write('Whoops! File not found!')
    } else {
      response.write(data)
      response.end()
    }
  })
})

app.get('/register', checkNotAuthenticated, function (req, response) {
  fs.readFile('register.html', null, function (error, data) {
    if (error) {
      response.writeHead(404)
      response.write('Whoops! File not found!')
    } else {
      response.write(data)
      response.end()
    }
  })
})

app.post('/logincode', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) { return next(err) }

    var output = {}
    if (!user) {
      output.status = false
      output.message = info.message
      return res.json(output)
    }
    req.logIn(user, function (err) {
      if (err) { return next(err) }
      output.status = true
      output.message = 'login successful'
      return res.json(output)
    })
  })(req, res, next)
})

app.post('/register', async (req, res) => {
  const hashedPassword = await bcrypt.hash(req.body.password, 10)// password is encoded

  await Mydb.users
    .create({ name: req.body.name, email: req.body.email, password: hashedPassword, type: 'user' })// for registering email id and add to user table
    .then(result => {
      res.redirect('/login')// if registering is succssesfull go to login
      res.end()
    })
    .catch(error => {
      console.log(error)
      res.redirect('/register')
    })
})

app.post('/logout', (req, res) => {
  req.logOut()// passport gives logout method
  res.redirect('/login')// after logout goto login page again
})

function checkAuthenticated (req, res, next) { // check is authenticated user or not
  if (req.isAuthenticated()) { // method given by passport
    return next()
  }

  res.redirect('/login')
}
function checkNotAuthenticated (req, res, next) { //
  if (req.isAuthenticated()) {
    return res.redirect('/')
  }
  next()
}
app.listen(1585)
module.exports = app
